package com.petproject.chessbot.core;

import com.petproject.chessbot.core.figures.*;
import com.petproject.dfs.ModelStateFactory;

public class ChessFactory implements ModelStateFactory<GameState> {
    @Override
    public GameState createState() {
        GameState gameState = new GameState();
        King blackKing = new King();
        blackKing.setSide(Side.BLACK);
        blackKing.setCoordinates(new Coordinates().withX(1).withY(Coordinates.MAX_Y));
        King whiteKing = new King();
        whiteKing.setSide(Side.WHITE);
        whiteKing.setCoordinates(new Coordinates().withX(3).withY(Coordinates.MAX_Y - 1));
        Pawn blackPawn = new Pawn();
        blackPawn.setSide(Side.BLACK);
        blackPawn.setCoordinates(new Coordinates().withX(1).withY(Coordinates.MAX_Y - 1));
        Pawn blackPawn2 = new Pawn();
        blackPawn2.setSide(Side.BLACK);
        blackPawn2.setCoordinates(new Coordinates().withX(2).withY(Coordinates.MAX_Y - 1));
        Bishop blackBishop = new Bishop();
        blackBishop.setSide(Side.BLACK);
        blackBishop.setCoordinates(new Coordinates().withX(3).withY(Coordinates.MAX_Y));
        Rook whiteRook = new Rook();
        whiteRook.setSide(Side.WHITE);
        whiteRook.setCoordinates(new Coordinates().withX(2).withY(1));
        Queen whiteQueen = new Queen();
        whiteQueen.setSide(Side.WHITE);
        whiteQueen.setCoordinates(new Coordinates().withX(6).withY(Coordinates.MAX_Y-2));
        gameState.getWhite().add(whiteKing);
        gameState.getWhite().add(whiteQueen);
        gameState.getWhite().add(whiteRook);
        gameState.getBlack().add(blackKing);
        gameState.getBlack().add(blackPawn);
        gameState.getBlack().add(blackPawn2);
        gameState.getBlack().add(blackBishop);
        gameState.setNext(Side.WHITE);
        return gameState;
    }
}
