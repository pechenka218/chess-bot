package com.petproject.chessbot.core;

import com.petproject.dfs.Turn;

public interface ChessTurn extends Turn {
    Figure figure();
    Coordinates oldCoordinates();
    Coordinates newCoordinates();
}
