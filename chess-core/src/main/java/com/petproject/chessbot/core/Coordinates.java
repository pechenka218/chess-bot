package com.petproject.chessbot.core;

public class Coordinates {
    private String[] LETTERS = {"A","B","C","D","E","F","G","H"};
    public static final int MAX_X = 8;
    public static final int MAX_Y = 8;
    private int x;
    private int y;

    public boolean isValid() {
        return x >= 1 && x <= MAX_Y && y >= 1 && y <= MAX_Y;
    }

    public Coordinates withX(int x) {
        setX(x);
        return this;
    }

    public Coordinates withY(int y) {
        setY(y);
        return this;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "["+LETTERS[x-1]+";"+y+"]";
    }
}
