package com.petproject.chessbot.core;

import java.util.List;

public abstract class Figure {
    protected Coordinates coordinates;
    protected boolean isKing = false;
    protected Side side;

    public abstract List<Coordinates> availableSteps();

    public List<Coordinates> availableStepsToKill(){
        return availableSteps();
    }

    public boolean isKing() {
        return isKing;
    }

    public void setKing(boolean king) {
        isKing = king;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public boolean isThrough(){
        return false;
    }
}
