package com.petproject.chessbot.core;

import com.petproject.chessbot.core.figures.King;
import com.petproject.chessbot.core.figures.Pawn;
import com.petproject.chessbot.core.util.Math;
import com.petproject.dfs.Model;
import com.petproject.dfs.Turn;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameSession implements Model<GameState> {
    private boolean checkmate(GameState state, Side side) {
        if (!check(state, side)) {
            return false;
        }
        for (Turn turn : availableTurns(state)) {
            turn.performTurn();
            if (!check(state, side)) {
                turn.revertTurn();
                return false;
            }
            turn.revertTurn();
        }
        return true;
    }

    private boolean check(GameState state, Side side) {
        List<Figure> figures = state.getFigures(side);
        Figure king = state.getKing(side.opposite());

        for (Figure figure : figures) {
            loop:
            for (Coordinates coordinates : figure.availableStepsToKill()) {
                if (king.getCoordinates().equals(coordinates)) {
                    if (!figure.isThrough()) {
                        int difX = king.getCoordinates().getX() - figure.getCoordinates().getX();
                        int difY = king.getCoordinates().getY() - figure.getCoordinates().getY();
                        int stepX = Math.signum(difX);
                        int stepY = Math.signum(difY);
                        int curX = figure.getCoordinates().getX();
                        int curY = figure.getCoordinates().getY();
                        while (curX != king.getCoordinates().getX() - stepX || curY != king.getCoordinates().getY() - stepY) {
                            curX += stepX;
                            curY += stepY;
                            if (state.getCoordinates().contains(new Coordinates().withX(curX).withY(curY))) {
                                continue loop;
                            }
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }


    private boolean isValidMove(GameState state, ChessTurn turn) {
        boolean result = true;
        if (!turn.figure().isThrough()) {
            int difX = turn.newCoordinates().getX() - turn.oldCoordinates().getX();
            int difY = turn.newCoordinates().getY() - turn.oldCoordinates().getY();
            int stepX = Math.signum(difX);
            int stepY = Math.signum(difY);
            int curX = turn.oldCoordinates().getX();
            int curY = turn.oldCoordinates().getY();
            while (curX != turn.newCoordinates().getX() - stepX || curY != turn.newCoordinates().getY() - stepY) {
                curX += stepX;
                curY += stepY;
                if (state.getCoordinates().contains(new Coordinates().withX(curX).withY(curY))) {
                    result = false;
                }
            }
        }
        if (result) {
            if (turn.figure() instanceof Pawn) {
                if ((turn.newCoordinates().getX() - turn.oldCoordinates().getX() != 0) &&
                        (state.getFiguresMap().get(turn.newCoordinates()) == null ||
                                state.getFiguresMap().get(turn.newCoordinates()).getSide().equals(turn.figure().getSide()))) {
                    result = false;
                }
            }
        }
        turn.performTurn();
        if (result) {
            Set<Coordinates> white = new HashSet<>();
            for (Figure figure : state.getWhite()) {
                if (white.contains(figure.getCoordinates())) {
                    result = false;
                    break;
                }
                white.add(figure.getCoordinates());
                if (!figure.getCoordinates().isValid()) {
                    result = false;
                    break;
                }
            }
        }
        if (result) {
            Set<Coordinates> black = new HashSet<>();
            for (Figure figure : state.getBlack()) {
                if (black.contains(figure.getCoordinates())) {
                    result = false;
                    break;
                }
                black.add(figure.getCoordinates());
                if (!figure.getCoordinates().isValid()) {
                    result = false;
                    break;
                }
            }
        }
        if (result) {
            if (check(state, state.getNext())) {
                result = false;
            }
        }
        turn.revertTurn();
        return result;
    }

    @Override
    public Iterable<Turn> availableTurns(GameState state) {
        List<Turn> turns = new ArrayList<>();
        List<Figure> figures = new ArrayList<>(state.getFigures(state.getNext()));
        for (Figure figure : figures) {
            if (figure.getSide().equals(state.getNext())) {
                for (Coordinates coordinates : figure.availableSteps()) {
                    ChessTurn turn = new ChessTurn() {
                        Coordinates old = figure.getCoordinates();
                        private Figure deleted;

                        @Override
                        public Figure figure() {
                            return figure;
                        }

                        @Override
                        public Coordinates oldCoordinates() {
                            return old;
                        }

                        @Override
                        public Coordinates newCoordinates() {
                            return coordinates;
                        }

                        @Override
                        public void performTurn() {
                            Figure figureToKill = state.getFiguresMap().get(coordinates);
                            if (figureToKill != null && !(figureToKill instanceof King) && (!figure.getSide().equals(figureToKill.getSide()))) {
                                deleted = figureToKill;
                                state.getFigures(figureToKill.getSide()).remove(figureToKill);
                            }
                            figure.setCoordinates(coordinates);
                            state.setNext(state.getNext().opposite());
                            state.getCoordinates().remove(old);
                            state.getCoordinates().add(coordinates);
                        }

                        @Override
                        public void revertTurn() {
                            if (deleted != null) {
                                state.getFigures(deleted.getSide()).add(deleted);
                            }
                            figure.setCoordinates(old);
                            state.setNext(state.getNext().opposite());
                            state.getCoordinates().remove(coordinates);
                            state.getCoordinates().add(old);
                        }

                        @Override
                        public String message() {
                            return String.format("Ходим %s %s из %s в %s", figure.getSide(), figure.getClass().getSimpleName(), old, coordinates);
                        }
                    };
                    if (isValidMove(state, turn)) {
                        turns.add(turn);
                    }
                }
            }
        }
        return turns;
    }

    @Override
    public boolean doneCriteria(GameState state) {
        return checkmate(state, Side.WHITE);
    }
}
