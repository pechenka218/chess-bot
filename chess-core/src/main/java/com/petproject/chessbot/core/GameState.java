package com.petproject.chessbot.core;

import com.petproject.chessbot.core.figures.King;
import com.petproject.dfs.ModelState;

import java.util.*;

public class GameState extends ModelState {
    private Side next;
    private List<Figure> black = new ArrayList<>();
    private List<Figure> white = new ArrayList<>();
    private Set<Coordinates> coordinates = new HashSet<>();

    public Figure getKing(Side side) {
        for (Figure figure : getFigures(side)) {
            if (figure instanceof King) {
                return figure;
            }
        }
        return null;
    }

    public Map<Coordinates,Figure> getFiguresMap(){
        Map<Coordinates,Figure> result = new HashMap<>();
        for (Figure figure : black) {
            if(result.keySet().contains(figure.getCoordinates())){
                throw new RuntimeException("Две фигуры с одинаковыми координатами");
            }
            result.put(figure.getCoordinates(),figure);
        }
        for (Figure figure : white) {
            if(result.keySet().contains(figure.getCoordinates())){
                throw new RuntimeException("Две фигуры с одинаковыми координатами");
            }
            result.put(figure.getCoordinates(),figure);
        }
        return result;
    }

    public List<Figure> getFigures(Side side) {
        return Side.BLACK == side ? getBlack() : getWhite();
    }

    public Side getNext() {
        return next;
    }

    public void setNext(Side next) {
        this.next = next;
    }

    public List<Figure> getBlack() {
        return black;
    }

    public void setBlack(List<Figure> black) {
        this.black = black;
    }

    public List<Figure> getWhite() {
        return white;
    }

    public void setWhite(List<Figure> white) {
        this.white = white;
    }

    public Set<Coordinates> getCoordinates() {
        Set<Coordinates> coordinates = new HashSet<>();
        for (Figure figure : white) {
            coordinates.add(figure.getCoordinates());
        }
        for (Figure figure : black) {
            coordinates.add(figure.getCoordinates());
        }
        return coordinates;
    }

    public void setCoordinates(Set<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }
}
