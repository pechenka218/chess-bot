package com.petproject.chessbot.core;

public enum Side {
    BLACK {
        @Override
        public int forwardFactor() {
            return -1;
        }

        @Override
        public Side opposite() {
            return Side.WHITE;
        }
    }, WHITE {
        @Override
        public int forwardFactor() {
            return 1;
        }

        @Override
        public Side opposite() {
            return Side.BLACK;
        }
    };

    public abstract int forwardFactor();

    public abstract Side opposite();
}
