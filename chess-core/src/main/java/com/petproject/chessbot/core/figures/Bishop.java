package com.petproject.chessbot.core.figures;

import com.petproject.chessbot.core.Coordinates;

import java.util.ArrayList;
import java.util.List;

public class Bishop extends ContinuousMoveFigure {
    @Override
    public List<Coordinates> availableSteps() {
        List<Coordinates> result = new ArrayList<>();
        addDiagonalMoves(result);
        return result;
    }
}
