package com.petproject.chessbot.core.figures;

import com.petproject.chessbot.core.Coordinates;
import com.petproject.chessbot.core.Figure;

import java.util.List;

public abstract class ContinuousMoveFigure extends Figure {
    protected void addDiagonalMoves(List<Coordinates> addTo) {
        addDiagonal(addTo, 1, 1);
        addDiagonal(addTo, 1, -1);
        addDiagonal(addTo, -1, 1);
        addDiagonal(addTo, -1, -1);
    }

    private void addDiagonal(List<Coordinates> addTo, int difX, int difY) {
        int curX = coordinates.getX();
        int curY = coordinates.getY();
        while (curX >= 1 && curX <= Coordinates.MAX_X && curY >= 1 && curY <= Coordinates.MAX_Y) {
            curX += difX;
            curY += difY;
            addTo.add(new Coordinates().withX(curX).withY(curY));
        }
    }

    protected void addLineMoves(List<Coordinates> addTo) {
        addDiagonal(addTo, -1, 0);
        addDiagonal(addTo, 1, 0);
        addDiagonal(addTo, 0, 1);
        addDiagonal(addTo, 0, -1);
    }
}
