package com.petproject.chessbot.core.figures;

import com.petproject.chessbot.core.Coordinates;
import com.petproject.chessbot.core.Figure;

import java.util.ArrayList;
import java.util.List;

public class King extends Figure {
    private int[][] steps = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

    public King() {
        super();
        isKing = true;
    }

    @Override
    public List<Coordinates> availableSteps() {
        List<Coordinates> result = new ArrayList<>();
        for (int[] ints : steps) {
            Coordinates coordinates = new Coordinates();
            coordinates.setX(this.coordinates.getX() + ints[0]);
            coordinates.setY(this.coordinates.getY() + ints[1]);
            result.add(coordinates);
        }
        return result;
    }
}
