package com.petproject.chessbot.core.figures;

import com.petproject.chessbot.core.Coordinates;
import com.petproject.chessbot.core.Figure;

import java.util.ArrayList;
import java.util.List;

public class Knight extends Figure {
    private int[][] steps = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, 2}, {1, -2}, {-1, 2}, {-1, -2}};

    @Override
    public List<Coordinates> availableSteps() {
        List<Coordinates> result = new ArrayList<>();
        for (int[] ints : steps) {
            Coordinates coordinates = new Coordinates();
            coordinates.setX(this.coordinates.getX() + ints[0]);
            coordinates.setY(this.coordinates.getY() + ints[1]);
            result.add(coordinates);
        }
        return result;
    }

    @Override
    public boolean isThrough() {
        return true;
    }
}
