package com.petproject.chessbot.core.figures;

import com.petproject.chessbot.core.Coordinates;
import com.petproject.chessbot.core.Figure;
import com.petproject.chessbot.core.Side;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Pawn extends Figure {
    @Override
    public List<Coordinates> availableSteps() {
        List<Coordinates> result = new ArrayList<>();
        result.add(forward(1));
        if (isStartPosition()) {
            result.add(forward(2));
        }
        result.addAll(availableStepsToKill());
        return result;
    }

    @Override
    public List<Coordinates> availableStepsToKill() {
        Coordinates direction1 = forward(1);
        direction1.setX(coordinates.getX() - 1);
        Coordinates direction2 = forward(1);
        direction2.setX(coordinates.getX() + 1);
        return Arrays.asList(direction1,direction2);
    }

    private Coordinates forward(int forward) {
        Coordinates newCoordinates = new Coordinates();
        newCoordinates.setX(coordinates.getX());
        newCoordinates.setY(coordinates.getY() + forward * side.forwardFactor());
        return newCoordinates;
    }

    private boolean isStartPosition() {
        if (Side.WHITE == side) {
            return coordinates.getY() == 1;
        } else {
            return coordinates.getY() == Coordinates.MAX_Y - 1;
        }
    }
}
