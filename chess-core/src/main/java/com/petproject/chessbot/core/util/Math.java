package com.petproject.chessbot.core.util;

public class Math {
    public static int signum(int x) {
        return Integer.compare(x, 0);
    }
}
