package com.petproject.dfs;

public interface Model<T extends ModelState> {
    Iterable<Turn> availableTurns(T state);
    boolean doneCriteria(T state);
}
