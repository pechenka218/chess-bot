package com.petproject.dfs;

public interface ModelStateFactory<T extends ModelState> {
    T createState();
}
