package com.petproject.dfs;

public class SearchModule<T extends ModelState> {
    private Model<T> model;
    private int turns = 4;
    private SearchResult searchResult = new SearchResult();
    int recursive = 0;

    public SearchResult depthSearch(T state) {
        for (Turn turn : model.availableTurns(state)) {
            recursive(state, turn);
            if (searchResult.isSuccess()) {
                return searchResult;
            }
        }
        return searchResult;
    }

    private void recursive(T state, Turn turn) {
        turn.performTurn();
        recursive++;
        searchResult.getTurns().add(turn);
        turns--;
//        for (int i = 0; i < recursive; i++) {
//            System.out.print(" ");
//        }
//        System.out.println(turn.message() + turns);
        if (model.doneCriteria(state)) {
            searchResult.setSuccess(true);
            return;
        }
        if (turns > 0) {
            for (Turn turn1 : model.availableTurns(state)) {
                recursive(state, turn1);
                if(searchResult.isSuccess()){
                    return;
                }
            }
        }
        recursive--;
        searchResult.getTurns().remove(searchResult.getTurns().size()-1);
        turn.revertTurn();
        turns++;
    }

    public Model<T> getModel() {
        return model;
    }

    public void setModel(Model<T> model) {
        this.model = model;
    }

    public SearchResult getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(SearchResult searchResult) {
        this.searchResult = searchResult;
    }
}
