package com.petproject.dfs;

import java.util.ArrayList;
import java.util.List;

public class SearchResult {
    private boolean success;
    private List<Turn> turns = new ArrayList<>();
    private ModelState finishState;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public ModelState getFinishState() {
        return finishState;
    }

    public void setFinishState(ModelState finishState) {
        this.finishState = finishState;
    }
}
