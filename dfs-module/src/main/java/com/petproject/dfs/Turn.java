package com.petproject.dfs;

public interface Turn {
    void performTurn();
    void revertTurn();
    String message();
}
