package com.petproject;

import com.petproject.chessbot.core.ChessFactory;
import com.petproject.chessbot.core.GameSession;
import com.petproject.chessbot.core.GameState;
import com.petproject.dfs.SearchModule;
import com.petproject.dfs.SearchResult;
import com.petproject.dfs.Turn;

public class Main {
    public static void main(String[] args) {
        GameState gameState = new ChessFactory().createState();
        SearchModule searchModule = new SearchModule();
        GameSession gameSession = new GameSession();
        searchModule.setModel(gameSession);
        SearchResult searchResult = searchModule.depthSearch(gameState);
        System.out.println(searchResult.isSuccess() ? "Мат найден" : "Мат не найден");
        for (Turn turn : searchResult.getTurns()) {
            System.out.println(turn.message());
        }
    }
}
